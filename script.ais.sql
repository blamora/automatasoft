SET search_path = public;

DROP table if exists cliente, tercero, producto, factura, detalle_factura cascade;
DROP SEQUENCE if exists "cliente_codigo_seq", "tercero_codigo_seq", "producto_idproducto_seq", "factura_codigo_seq", "det_factura_codigo_seq";

CREATE TABLE cliente
(
    codigo integer NOT NULL,
    nombre character varying(50),
    apellido character varying(50),
    direccion character varying(50),
    email character varying(50),
    CONSTRAINT "cliente_pkey" PRIMARY KEY (codigo)
);

ALTER TABLE cliente OWNER to postgres;

CREATE SEQUENCE cliente_codigo_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
    
ALTER TABLE "cliente_codigo_seq" OWNER to postgres;

ALTER SEQUENCE cliente_codigo_seq OWNED BY cliente.codigo;

ALTER TABLE ONLY cliente ALTER COLUMN codigo SET DEFAULT nextval('cliente_codigo_seq'::regclass);

CREATE TABLE tercero
(
    codigo integer NOT NULL,
    nombre character varying(50),
    apellido character varying(50),
    cargo character varying(50),
    CONSTRAINT "tercero_pkey" PRIMARY KEY (codigo)
);

ALTER TABLE tercero OWNER to postgres;

CREATE SEQUENCE tercero_codigo_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER TABLE "tercero_codigo_seq" OWNER to postgres;

ALTER SEQUENCE tercero_codigo_seq OWNED BY Tercero.codigo;

ALTER TABLE ONLY tercero ALTER COLUMN codigo SET DEFAULT nextval('tercero_codigo_seq'::regclass);



CREATE TABLE producto
(
  codigo integer NOT NULL,
  descripcion character varying(100),
  cantidad integer,
  precio float,
  CONSTRAINT "producto_pkey" PRIMARY KEY (codigo)
);

ALTER TABLE producto OWNER to postgres;

CREATE SEQUENCE producto_idproducto_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER TABLE "producto_idproducto_seq" OWNER to postgres;

ALTER SEQUENCE producto_idproducto_seq OWNED BY producto.codigo;

ALTER TABLE ONLY producto ALTER COLUMN codigo SET DEFAULT nextval('producto_idproducto_seq'::regclass);


CREATE TABLE factura
(
  codigo integer NOT NULL,
  fecha date,
  id_tercero integer,
  id_cliente integer,
  CONSTRAINT "factura_pkey" PRIMARY KEY (codigo)
);

ALTER TABLE factura OWNER to postgres;

CREATE SEQUENCE factura_codigo_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER TABLE "factura_codigo_seq" OWNER to postgres;

ALTER SEQUENCE factura_codigo_seq OWNED BY factura.codigo;

ALTER TABLE ONLY factura ALTER COLUMN codigo SET DEFAULT nextval('factura_codigo_seq'::regclass);

ALTER TABLE ONLY factura ADD CONSTRAINT "factura_id_cliente_fkey" FOREIGN KEY (id_cliente)
      REFERENCES public.cliente (codigo);
ALTER TABLE ONLY factura ADD CONSTRAINT "factura_id_tercero_fkey" FOREIGN KEY (id_tercero)
      REFERENCES public.tercero (codigo);


CREATE TABLE detalle_factura
(
  codigo integer NOT NULL,
  id_producto integer,
  cantidad integer,
  precio_total float,
  id_factura integer,
  CONSTRAINT detalle_factura_pkey PRIMARY KEY (codigo)
);

ALTER TABLE detalle_factura OWNER to postgres;

CREATE SEQUENCE det_factura_codigo_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER TABLE "det_factura_codigo_seq" OWNER to postgres;

ALTER SEQUENCE det_factura_codigo_seq OWNED BY detalle_factura.codigo;

ALTER TABLE ONLY detalle_factura ALTER COLUMN codigo SET DEFAULT nextval('det_factura_codigo_seq'::regclass);

ALTER TABLE only detalle_factura add CONSTRAINT "factura_id_det_factura_fkey" FOREIGN KEY (id_factura)
      REFERENCES public.factura (codigo);
      
ALTER TABLE only detalle_factura add CONSTRAINT "producto_id_det_factura_fkey" FOREIGN KEY (id_producto)
      REFERENCES public.producto (codigo);

